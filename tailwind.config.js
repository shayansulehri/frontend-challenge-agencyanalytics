/** @type {import('tailwindcss').Config} */
module.exports = {
  content: ['./src/**/*.{js,jsx,ts,tsx}'],
  theme: {
    extend: {
      colors: {
        // Custom Colors
        AASky: '#eef6fb',
        AABlue: '#5fb0e8',
        AADarkBlue: '#2e4369',
        AABlack: '#333339',
      },
    },
  },
  plugins: [],
};
