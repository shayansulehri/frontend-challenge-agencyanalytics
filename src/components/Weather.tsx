import { useState } from 'react';
import useFetchWeather, { IWeather } from '../api/weatherService';
import useFetchGeoLocation, { IGeolocation } from '../api/geolocationService';
import { capitalizeFirstLetter } from '../utils';

type ICity = {
  name: string;
  country: string;
};
//List of cities
const cities: ICity[] = [
  {
    name: 'vancouver',
    country: 'CA',
  },
  {
    name: 'toronto',
    country: 'CA',
  },
  {
    name: 'lahore',
    country: 'PK',
  },
];
export default function Weather() {
  const [currentCity, setCurrentCity] = useState<ICity>(cities[0]);

  const geoLocation: IGeolocation = useFetchGeoLocation(currentCity.name, currentCity.country);
  const { data: weather, loading, error } = useFetchWeather(geoLocation.lat, geoLocation.lon);

  const fetchWeather = (cityName: string, countryCode: string) => {
    setCurrentCity({ name: cityName, country: countryCode });
  };
  return (
    <>
      <div className='flex items-stretch justify-between h-16 mt-5 nav'>
        <ul className='flex items-center text-xl md:text-3xl font-extralight gap-14 font-face-montserrat-light'>
          {cities.map((city: any) => (
            <li key={city.name} className='cursor-pointer'>
              <span
                className={`${currentCity.name === city.name ? 'active' : ''}`}
                onClick={() => fetchWeather(city.name, city.country)}>
                {city.name.toUpperCase()}
              </span>
            </li>
          ))}
        </ul>
      </div>

      {!loading && (
        <div className='w-3/4 m-5 border-4 border-white shadow-xl rounded-xl'>
          {weather && weather[0] ? (
            <div className='flex flex-col items-center p-8 my-3 space-y-5'>
              <span className='text-3xl font-light font-face-montserrat-light'>{weather[0].day}</span>
              <div className='flex items-start space-x-5'>
                <div className='text-7xl md:text-9xl'>
                  <i
                    className={`text-AADarkBlue wi ${weather[0].night ? 'wi-owm-night-' : 'wi-owm-'}${
                      weather[0].id
                    }`}></i>
                </div>
                <div className='flex flex-col mt-2'>
                  <span className='leading-none md:text-[80px] text-5xl font-face-teko'>
                    {Math.round(weather[0].temp)}°
                  </span>
                  <p className='md:text-[26px] text-xl leading-none font-light font-face-montserrat-light'>
                    {capitalizeFirstLetter(weather[0].main)}
                  </p>
                </div>
              </div>
            </div>
          ) : (
            ''
          )}

          {/* Forecast */}
          <div className='border-t-4 border-white'>
            <div className='grid grid-cols-1 md:grid-cols-4'>
              {weather &&
                weather.slice(1).map((w: IWeather, index: number) => (
                  <div
                    key={w.day}
                    className={`w-full ${index === weather.length - 2 ? '' : 'md:border-white md:border-r-4'}`}>
                    <div className='flex flex-col items-center p-5 space-y-5 cursor-pointer'>
                      <span className='text-2xl font-light font-face-montserrat-light'>{w.day}</span>
                      <div className='text-5xl'>
                        <i className={`text-AADarkBlue wi ${w.night ? 'wi-owm-night-' : 'wi-owm-'}${w.id}`}></i>
                      </div>
                      <div className=''>
                        <span className='text-4xl font-face-teko'> {Math.round(w.temp)}°</span>
                      </div>
                    </div>
                  </div>
                ))}
            </div>
          </div>
        </div>
      )}
      {error && <div>{error}</div>}
    </>
  );
}
