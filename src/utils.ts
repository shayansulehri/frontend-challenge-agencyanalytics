const daysOfWeekMap = {
  0: 'Sun',
  1: 'Mon',
  2: 'Tue',
  3: 'Wed',
  4: 'Thu',
  5: 'Fri',
  6: 'Sat',
};
export const getDayName = (unixTimestamp: number) => {
  type ObjectKey = keyof typeof daysOfWeekMap;
  const numberOfDayInWeek = ((Math.floor(unixTimestamp / 86400) + 4) % 7) as ObjectKey; // e.g day 2 is Tuesday
  return daysOfWeekMap[numberOfDayInWeek];
};

export const capitalizeFirstLetter = (str: string) => {
  const arr = str.split(' ');
  for (var i = 0; i < arr.length; i++) {
    arr[i] = arr[i].charAt(0).toUpperCase() + arr[i].slice(1);
  }
  return arr.join(' ');
  //   return string.charAt(0).toUpperCase() + string.slice(1);
};
