import { useState, useEffect } from 'react';

export type IGeolocation = {
  lat: number;
  lon: number;
};
const useFetchGeoLocation = (cityName: string, countryCode: string) => {
  const [geolocation, setGeolocation] = useState<IGeolocation>({ lat: 0, lon: 0 });

  const APP_ID = process.env.REACT_APP_OWM_API_KEY;
  const url = `https://api.openweathermap.org/geo/1.0/direct?q=${cityName},${countryCode}&appid=${APP_ID}`;

  useEffect(() => {
    fetch(url)
      .then((res) => {
        if (res.ok) {
          return res.json();
        }
        throw res;
      })
      .then((data) => {
        if (data && data[0]) {
          setGeolocation({ lat: data[0].lat, lon: data[0].lon });
        }
      })
      .catch((error) => {
        console.error('Error fetching Geolocation data', error);
      });
  }, [cityName, countryCode, url]);
  return geolocation;
};
export default useFetchGeoLocation;
