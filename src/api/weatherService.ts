import { useState, useEffect } from 'react';
import { getDayName } from '../utils';

export type IWeather = {
  day: string;
  temp: number;
  id: string;
  description: string;
  main: string;
  night: boolean;
};
const useFetchWeather = (lat: number, lon: number) => {
  const APP_ID = process.env.REACT_APP_OWM_API_KEY;
  const url = `https://api.openweathermap.org/data/2.5/onecall?lat=${lat.toString()}&lon=${lon.toString()}&exclude=hourly,minutely&units=metric&appid=${APP_ID}`;

  const [data, setData] = useState<IWeather[]>([]);
  const [loading, setLoading] = useState(false);
  const [error, setError] = useState(null);

  useEffect(() => {
    setLoading(true);
    if (lat !== 0 && lon !== 0) {
      fetch(url)
        .then((res) => res.json())
        .then((data) => {
          const forecast: IWeather[] = [];
          if (data.current) {
            const current: IWeather = {
              day: 'Today',
              temp: data.current.temp,
              description: data.current.weather && data.current.weather[0] ? data.current.weather[0].description : '',
              main: data.current.weather && data.current.weather[0] ? data.current.weather[0].main : '',
              id: data.current.weather && data.current.weather[0] ? data.current.weather[0].id : '',
              night: data.current.dt < data.current.sunrise || data.current.dt > data.current.sunset ? true : false,
            };
            forecast.push(current);
          }
          if (data.daily) {
            for (let i = 1; i < 5; i++) {
              const dailyData: IWeather = {
                day: getDayName(data.daily[i].dt),
                temp: (data.daily[i].temp.min + data.daily[i].temp.max) / 2, //mean temp: between max and min temp
                description:
                  data.daily[i].weather && data.daily[i].weather[0] ? data.daily[i].weather[0].description : '',
                main: data.daily[i].weather && data.daily[i].weather[0] ? data.daily[i].weather[0].main : '',
                id: data.daily[i].weather && data.daily[i].weather[0] ? data.daily[i].weather[0].id : '',
                night:
                  data.daily[i].dt < data.daily[i].sunrise || data.daily[i].dt > data.daily[i].sunset ? true : false,
              };
              forecast.push(dailyData);
            }
          }
          setData(forecast);
        })
        .catch((error) => {
          console.error('Error fetching Weather data', error);
          setError(error);
        })
        .finally(() => {
          setLoading(false);
        });
    }
  }, [lat, lon, url]);
  return { data, loading, error };
};
export default useFetchWeather;
