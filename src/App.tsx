import React from 'react';
import Weather from './components/Weather';
function App() {
  return (
    <div className='App'>
      <div className='antialiase bg-AASky text-AABlack'>
        <div className='container min-h-screen mx-auto md:w-2/3'>
          <div className='flex flex-col items-center justify-center'>
            <Weather />
          </div>
        </div>
      </div>
    </div>
  );
}

export default App;
